#!/bin/sh

set -e

user="vagrant"
key="id_autocluster"

u_ssh="/home/${user}/.ssh"
r_ssh="/root/.ssh"

mkdir -p "$r_ssh"
chmod 700 "$r_ssh"
cp "${u_ssh}/${key}" "${r_ssh}/"
cp "${u_ssh}/${key}.pub" "${r_ssh}/"
cat "${u_ssh}/${key}.pub" >> "${r_ssh}/authorized_keys"
if selinuxenabled >/dev/null 2>&1 ; then
	#chcon -t ssh_home_t "${r_ssh}/authorized_keys"
	restorecon -R "$r_ssh"
fi
cat "${u_ssh}/${key}.pub" >> "${u_ssh}/authorized_keys"
if selinuxenabled >/dev/null 2>&1 ; then
	#chcon -t ssh_home_t "${u_ssh}/authorized_keys"
	restorecon -R "$u_ssh"
fi
