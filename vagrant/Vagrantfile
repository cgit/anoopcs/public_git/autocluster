# -*- mode: ruby -*-
# vi: ft=ruby:et:ts=2:sts=2:sw=2

VAGRANTFILE_API_VERSION = "2"

require 'yaml'
require 'ipaddr'
require 'resolv'
require 'securerandom'

f = ENV['AUTOCLUSTER_STATE'] + '/config.yml'
if File.exists?(f)
  settings = YAML::load_file f

  puts "Loaded config from #{f}."
end

def resolvURL(url)
  u = URI.parse(url)
  u.host = Resolv.getaddress(u.host)
  u.to_s
end

shared_disk_ids = []
for i in 1..settings['shared_disks']['count']
  shared_disk_ids[i] = sprintf('AUTO-%02d-', i) + SecureRandom.uuid[0..7]
end

#
# The vagrant machine definitions
#

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  if ENV['http_proxy'] or ENV['https_proxy']
    if Vagrant.has_plugin?("vagrant-proxyconf")
      if ENV['http_proxy']
        config.proxy.http = resolvURL(ENV['http_proxy'])
      end
      if ENV['http_proxys']
        config.proxy.https = resolvURL(ENV['http_proxys'])
      end
      config.proxy.no_proxy = "localhost,127.0.0.1"
    end
  end

  if Vagrant.has_plugin?("vagrant-libvirt")
    config.vm.provider :libvirt do |libvirt|
      libvirt.storage_pool_name = "autocluster"
    end
  end

  settings['nodes'].each do |hostname, node|
    config.vm.define hostname do |v|
      v.vm.box = settings['vagrant_box']
      v.vm.hostname = hostname

      node['ips'].each do |ip|
        v.vm.network "private_network",
                     ip: ip,
                     nm_controlled: "no"
      end

      if settings['virthost']
        virthost = settings['virthost']
        v.vm.provision "shell",
                       run: "always",
                       inline: "route add default gw " + virthost + "|| :"
      end

      # No shared folders - they might require extra software on the
      # nodes and installation can time out  :-(
      v.vm.synced_folder ".", "/vagrant", disabled: true

      v.vm.provision "file",
                     source: "~/.ssh/id_autocluster",
                     destination: "~/.ssh/id_autocluster"
      v.vm.provision "file",
                     source: "~/.ssh/id_autocluster.pub",
                     destination: "~/.ssh/id_autocluster.pub"
      v.vm.provision :shell,
                     privileged: true,
                     path: "autocluster_ssh_node_setup.sh"

      v.vm.provider :libvirt do |libvirt|

        libvirt.default_prefix = 'autocluster'
        libvirt.cpus = settings['cpus']
        #FIXME: causes an error ### libvirt.memory = settings['memory']

        if node['has_shared_storage']
          for i in 1..settings['shared_disks']['count']
            libvirt.storage :file,
                            :serial => shared_disk_ids[i],
                            :path => sprintf('autocluster_%s_shared%02d.img',
                                             settings['cluster'], i),
                            :size => settings['shared_disks'].size,
                            :allow_existing => true,
                            :shareable => true,
                            :type => 'raw'
          end
        end
      end

      # The libvirt provider sometimes configures a private network
      # but doesn't bring it up.  Check that all desired IPs are
      # assigned, failing if any are missing.
      v.vm.provision "shell",
                     run: "always",
                     path: "autocluster_check_ips.sh",
                     args: node['ips']
    end
  end
end
